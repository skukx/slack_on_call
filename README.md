# Slack On Call
[![pipeline status](https://gitlab.com/skukx/slack_on_call/badges/master/pipeline.svg)](https://gitlab.com/skukx/slack_on_call/commits/master)
[![coverage report](https://gitlab.com/skukx/slack_on_call/badges/master/coverage.svg)](https://gitlab.com/skukx/slack_on_call/commits/master)


This application helps manage a Slack user group to transition users in an out
of the group based on a schedule.

## Setup
```
bundle install
bundle exec rake db:create
bundle exec rake db:migrate

bundle exec rails s
```

## Testing
```
bundle exec rspec
```
