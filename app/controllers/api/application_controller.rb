# frozen_string_literal: true

module Api
  class ApplicationController < ActionController::API
    I18N_SCOPE = 'api'

    ##
    # Renders a successful json response with specified body and status.
    #
    # @example Send response without body
    #   respond_with_success
    #   # { "status": "success", "data": null }
    #
    # @example Send response with body
    #   respond_with_success body: { users: [{ id: 1, name: 'Joe' }] }
    #   # { "status": "success", "data": { "users": [...] } }
    #
    # @param [Hash] messages Messages sent back to client. (Must be a key value pair)
    # @param [Integer] status The status code.
    #
    def respond_with_success(body: nil, status: 200)
      unless body.nil? || body.is_a?(Hash)
        message = t(:invalid_message_format, class: Hash, wrong_class: body.class)
        return respond_with_error message: message
      end

      render status: status, json: { status: t(:status_success), data: body }
    end

    ##
    # Renders a failed json response with specified body and status.
    #
    # @example Send failed response with missing arguments.
    #   messages = { email: 'Email is required.', age: 'Must be 21 or older.' }
    #   respond_with_fail body: messages, status: 400
    #
    #   # { "status": "fail", "data": { "email": "Email is required.", "age": "Must be 21 or older" } }
    #
    # @param [Hash] messages Messages sent back to client. (Must be a key value pair)
    # @param [Integer] status The status code.
    #
    def respond_with_fail(body: {}, status: 400)
      unless body.is_a? Hash
        message = t(:invalid_message_format, class: Hash, wrong_class: body.class)
        return respond_with_error message: message
      end

      render status: status, json: { status: t(:status_fail), data: body }
    end

    ##
    # Renders an error json response with specified body and status.
    #
    # @example Send failed response with missing arguments.
    #   respond_with_error message: 'Unable to connect to database.'
    #   # { "status": "error", "message": "Unable to connect to database." }
    #
    # @param [Hash] message Message sent back to client. (Must be a string)
    # @param [Integer] status The status code.
    #
    def respond_with_error(message: '', status: 500)
      unless message.is_a? String
        message = t(:invalid_message_format, class: String, wrong_class: message.class)
        return respond_with_error message: message
      end

      render status: status, json: { status: t(:status_error), message: message }
    end

    def t(key, options = {})
      I18n.t(key, options.reverse_merge(scope: I18N_SCOPE))
    end

    def validate_args(*required_keys)
      error_messages = {}
      required_keys.each do |key|
        error_messages[key.to_sym] = t(:missing_required_param, param: key) if params[key].blank?
      end

      return respond_with_fail body: error_messages if error_messages.any?
    end
  end
end
