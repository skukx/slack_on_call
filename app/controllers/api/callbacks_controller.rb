# frozen_string_literal: true

module Api
  class CallbacksController < ApplicationController
    def temporize
      Schedules::UpdateService.new(params).execute
      respond_with_success
    end
  end
end
