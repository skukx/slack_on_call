# frozen_string_literal: true

module Api
  module Slack
    class ApplicationController < Api::ApplicationController
      before_action :build_slack_auth
      before_action :validate_verification_token
      before_action :set_calling_user
      before_action :set_default_response_format

      attr_reader :auth, :calling_user

      # rescue_from StandardError, with: :error_occurred

      protected

      def validate_verification_token
        respond_with_error unless auth.valid_token?
      end

      def set_calling_user
        @calling_user ||= User
                          .create_with(slack_name: auth.user.name)
                          .find_or_create_by!(slack_id: auth.user.id)
      end

      def error_occurred(error)
        Rails.logger.error error

        render json: {
          response_type: 'ephemeral',
          text: 'Something went wrong, please try again.',
          replace_original: false
        }
      end

      def set_default_response_format
        request.format = :json
      end

      private

      def build_slack_auth
        @auth ||= Chat::Slack::UserAuth.new(params)
      end
    end
  end
end
