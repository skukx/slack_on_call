# frozen_string_literal: true

module Api
  module Slack
    class CommandsController < ApplicationController
      before_action :received_action, only: [:actions]

      def actions
        @message = Actions::ReceiveService.new(received_action).execute
        render json: @message
      end

      def add_on_call
        @message = Chat::Slack::UserSelectionMessage.new
        render json: @message
      end

      def on_call_schedule
        @message = Schedules::OutputService.new(params, calling_user).execute
        render json: @message
      end

      def remove_on_call
        respond_with_success
      end

      protected

      def received_action
        @event ||= SlackEvent.create payload: ActionController::Parameters.new(
          JSON.parse(params[:payload])
        )
      end
    end
  end
end
