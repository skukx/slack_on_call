# frozen_string_literal: true

class BaseFormatter
  def output(_context)
    raise NotImplementedError
  end
end
