# frozen_string_literal: true

module SlackMessage
  class ScheduledUsersFormatter < BaseFormatter
    def output(context)
      text = "Scheduled for #{context.titleize}"
      Chat::Slack::Message.new(text: text, attachments: attachments(context))
    end

    private

    def attachments(context)
      scheduled_users = User.scheduled_on(context)
      texts = scheduled_users.map do |user|
        shifts = user.schedules.where(day_of_week: context.downcase).pluck(:shift)
        "<@#{user.slack_id}>: #{shifts.to_sentence}\n"
      end

      [Chat::Slack::Attachment.new(text: texts.join("\n"), color: 'good')]
    end
  end
end
