# frozen_string_literal: true

module SlackMessage
  class UserSchedulesFormatter < BaseFormatter
    def output(context)
      scheduled_days = context.schedules.group(:day_of_week).pluck(:day_of_week)
      text = "Schedule for <@#{context.slack_id}>\n"
      attachment = Chat::Slack::Attachment.new(
        text: output_shifts(context, scheduled_days),
        color: 'good'
      )

      message = Chat::Slack::Message.new text: text
      message.add_attachment(attachment)
      message
    end

    private

    def output_shifts(context, scheduled_days)
      attachment_text = ''
      scheduled_days.each do |day|
        shifts = context.schedules.where(day_of_week: day).pluck(:shift)
        attachment_text += "#{day.titlecase}: #{shifts.map(&:upcase).to_sentence}\n"
      end
      attachment_text
    end
  end
end
