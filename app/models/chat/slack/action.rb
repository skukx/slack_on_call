# frozen_string_literal: true

module Chat
  module Slack
    class Action < Base
      attr_accessor :name, :text, :type, :value, :confirm, :style, :options,
                    :option_groups, :data_source, :selected_options,
                    :min_query_length

      def initialize(args = {})
        assign_attributes(args)
        @options ||= []
        @selected_options ||= []

        selected_options.map! do |option|
          return option if option.is_a? Chat::Slack::Option
          Option.new option
        end
      end

      def add_option(option)
        @options << option
      end

      def remove_option(option)
        @options.delete(option)
      end

      def add_selected_option(option)
        @options << option
      end

      def remove_selected_option(option)
        @options.delete(option)
      end
    end
  end
end
