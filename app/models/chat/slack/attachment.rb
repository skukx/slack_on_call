# frozen_string_literal: true

module Chat
  module Slack
    class Attachment < Base
      attr_accessor :title, :fallback, :callback_id, :color, :actions,
                    :attachment_type, :text

      def initialize(args = {})
        assign_attributes(args)
        @actions ||= []
      end

      def add_action(action)
        @actions << action
      end

      def remove_action(action)
        @actions.delete(action)
      end
    end
  end
end
