# frozen_string_literal: true

module Chat
  module Slack
    class ButtonAction < Action
      def initialize(args = {})
        super
        @type = 'button'
      end
    end
  end
end
