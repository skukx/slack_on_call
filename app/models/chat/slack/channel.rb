# frozen_string_literal: true

module Chat
  module Slack
    class Channel < Identifier; end
  end
end
