# frozen_string_literal: true

module Chat
  module Slack
    class DaySelectionAction < Action
      def initialize
        super

        @name = 'day'
        @text = 'Select a Day'
        @type = 'select'

        Date::DAYNAMES.each do |day|
          add_option(Option.new(value: day.downcase, text: day))
        end
      end
    end
  end
end
