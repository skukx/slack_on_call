# frozen_string_literal: true

module Chat
  module Slack
    class DaySelectionAttachment < Attachment
      CALLBACK_ID = 'day_selection'

      def initialize
        super

        @text = 'Choose a Day'
        @callback_id = CALLBACK_ID
        add_action(DaySelectionAction.new)
      end
    end
  end
end
