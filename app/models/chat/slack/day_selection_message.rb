# frozen_string_literal: true

module Chat
  module Slack
    class DaySelectionMessage < Message
      def initialize
        super
        add_attachment(DaySelectionAttachment.new)
      end
    end
  end
end
