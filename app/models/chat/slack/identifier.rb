# frozen_string_literal: true

module Chat
  module Slack
    class Identifier
      attr_reader :id, :name

      def initialize(id, name)
        @id = id
        @name = name
      end

      def to_bold
        "*#{name}*"
      end
    end
  end
end
