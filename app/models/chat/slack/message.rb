# frozen_string_literal: true

module Chat
  module Slack
    class Message < Base
      attr_accessor :text, :attachments, :thread_ts, :response_type,
                    :replace_original, :delete_original, :type

      def initialize(args = {})
        assign_attributes(args)
        @attachments ||= []
      end

      def add_attachment(attachment)
        @attachments << attachment
      end

      def remove_attachment(attachment)
        @attachments.delete(attachment)
      end
    end
  end
end
