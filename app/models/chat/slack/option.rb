# frozen_string_literal: true

module Chat
  module Slack
    class Option < Base
      attr_accessor :text, :value

      def initialize(args = {})
        assign_attributes(args)
      end
    end
  end
end
