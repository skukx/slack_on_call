# frozen_string_literal: true

module Chat
  module Slack
    class ReceivedAction < Base
      attr_reader :payload

      def initialize(payload)
        @payload = payload
      end

      def actions
        @actions ||= payload[:actions].map do |action|
          Action.new action
        end
      end

      def callback_id
        payload[:callback_id]
      end

      def message_ts
        payload[:message_ts]
      end
    end
  end
end
