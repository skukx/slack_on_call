# frozen_string_literal: true

module Chat
  module Slack
    class Text < Base
      attr_reader :raw, :mentions, :mentioned_users, :mentioned_channels

      def initialize(text)
        @raw = text
        parse_mentions

        @mentioned_users = @mentions.select { |mention| mention.is_a? User }
        @mentioned_channels = @mentions.select { |mention| mention.is_a? Channel }
      end

      protected

      def parse_mentions
        parsed = raw.scan(/<(.*?)>/).flatten
        @mentions = parsed.map { |value| parse_mention_value(value) }.compact
      end

      def parse_mention_value(value)
        id, name = value.split('|')

        if user?(value) # User
          User.new(id, name)
        elsif channel?(value) # Channel
          Channel.new(id, name)
        end
      end

      def user?(token)
        token[0].eql?('@')
      end

      def channel?(token)
        token[0].eql?('#')
      end
    end
  end
end
