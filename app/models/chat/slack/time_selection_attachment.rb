# frozen_string_literal: true

module Chat
  module Slack
    class TimeSelectionAttachment < Attachment
      CALLBACK_ID = 'time_selection'
      SHIFTS = %w[
        am
        pm
      ].freeze

      def initialize
        super

        @text = 'Choose a Shift'
        @callback_id = CALLBACK_ID

        SHIFTS.each do |key|
          new_action = ButtonAction.new(text: key.upcase, value: key, name: 'time')
          add_action(new_action)
        end
      end
    end
  end
end
