# frozen_string_literal: true

module Chat
  module Slack
    class TimeSelectionMessage < Message
      def initialize
        super
        add_attachment(TimeSelectionAttachment.new)
      end
    end
  end
end
