# frozen_string_literal: true

module Chat
  module Slack
    class UserAuth
      def initialize(params = {})
        @params = params.dup
        return if params[:payload].nil?

        @payload = ActionController::Parameters.new JSON.parse(params[:payload])
      end

      def verification_token
        @verification_token ||= params[:token] || @payload.try(:[], :token)
      end

      def valid_token?
        verification_token == ENV['SLACK_VERIFICATION_TOKEN']
      end

      def user
        return @user if @user.present?

        id = params[:user_id] || @payload.try(:[], :user).try(:[], :id)
        name = params[:user_name] || @payload.try(:[], :user).try(:[], :name)
        @user = Chat::Slack::User.new(id, name)
      end

      private

      attr_reader :params
    end
  end
end
