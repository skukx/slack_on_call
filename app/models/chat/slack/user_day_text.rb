# frozen_string_literal: true

module Chat
  module Slack
    class UserDayText < Text
      def day?
        DateTime::DAYNAMES.include?(@raw.titleize)
      end

      def day
        return unless day?
        @day ||= DateTime::DAYNAMES.detect { |day| day.eql?(@raw.titleize) }
      end
    end
  end
end
