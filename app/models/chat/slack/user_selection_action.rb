# frozen_string_literal: true

module Chat
  module Slack
    class UserSelectionAction < Action
      def initialize
        @name = 'user'
        @text = 'Select a user'
        @type = 'select'
        @data_source = 'users'
      end
    end
  end
end
