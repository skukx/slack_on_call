# frozen_string_literal: true

module Chat
  module Slack
    class UserSelectionAttachment < Attachment
      CALLBACK_ID = 'user_selection'

      def initialize
        super

        @text = 'Choose a User'
        @callback_id = CALLBACK_ID
        add_action(UserSelectionAction.new)
      end
    end
  end
end
