# frozen_string_literal: true

module Chat
  module Slack
    class UserSelectionMessage < Message
      def initialize
        super
        add_attachment(UserSelectionAttachment.new)
      end
    end
  end
end
