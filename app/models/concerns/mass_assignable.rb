# frozen_string_literal: true

module MassAssignable
  extend ActiveSupport::Concern

  included do
    def assign_attributes(attributes)
      attributes.each { |key, value| public_send("#{key}=", value) }
    end
  end
end
