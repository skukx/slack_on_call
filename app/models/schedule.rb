# frozen_string_literal: true

class Schedule < ApplicationRecord
  enum day_of_week: %i[sunday monday tuesday wednesday thursday friday saturday]
  enum shift: %i[am pm]

  validates :user_id, presence: true
  validates :user_id, uniqueness: { scope: %i[day_of_week shift] }

  belongs_to :user

  def self.output_scheduled_users(day_of_week, formatter = nil)
    raise ArgumentError, 'Invalid Day' if DateTime::DAYNAMES.exclude?(day_of_week.titleize)

    formatter ||= SlackMessage::ScheduledUsersFormatter.new
    formatter.output(day_of_week)
  end
end
