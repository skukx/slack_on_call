# frozen_string_literal: true

class SlackEvent < ApplicationRecord
  validates :payload, presence: true

  scope :with_timestamp, lambda { |message_ts|
    where("payload ->> 'message_ts' = ?", message_ts)
  }

  scope :with_callback_id, lambda { |callback_id|
    where("payload ->> 'callback_id' = ?", callback_id)
  }

  scope :with_action, lambda { |action_name, idx = 0|
    where("payload #> '{actions,?}' ->> 'name' = ?", idx, action_name)
  }

  scope :with_channel, lambda { |channel_id|
    where("payload -> 'channel' ->> 'id' = ?", channel_id)
  }

  ##
  # Gets the callback_id of the slack event.
  #
  def callback_id
    payload['callback_id']
  end

  ##
  # Gets the slack message timestamp
  #
  def message_ts
    payload['message_ts']
  end

  ##
  # The user that tiggered that command
  # @return [Chat::Slack::User]
  #
  def caller
    @user ||= Chat::Slack::User.new(
      payload['user']['id'],
      payload['user']['name']
    )
  end

  ##
  # Actions associated with the event
  # @return [Array<Chat::Slack::Action>]
  #
  def actions
    @actions ||= payload.fetch('actions', []).map do |action|
      Chat::Slack::Action.new action
    end
  end

  ##
  # Gets a specific action by name
  # @param action_name [String] The name of the action to find.
  # @return [Chat::Slack::Action]
  #
  def action(action_name)
    return [] unless payload['actions']
    actions.detect { |action| action.name.eql? action_name }
  end

  ##
  # Get the selected options from an event
  # @param action_name [String] The name of the action to to get selected options for.
  # @return [Array<Chat::Slack::Option>]
  #
  def selected_options(action_name)
    action(action_name).try(:[], 'selected_options')
  end

  ##
  # Get a specific sibling event based on the callback_id
  # @param callback_id [String] The callback_id on the event to find.
  # @return [SlackEvent]
  #
  def sibling(callback_id)
    siblings.with_callback_id(callback_id).last
  end

  ##
  # Find other events associated with the message timestamp.
  # @return [Array<SlackEvent>]
  #
  def siblings
    self.class.with_timestamp(message_ts).where.not(id: id)
  end
end
