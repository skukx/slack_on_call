# frozen_string_literal: true

class User < ApplicationRecord
  validates :slack_id, presence: true, uniqueness: true

  has_many :schedules, dependent: :destroy

  scope :scheduled_on, lambda { |day_of_week|
    joins(:schedules).
      where(schedules: { day_of_week: day_of_week.downcase }).
      distinct
  }

  def output_schedule(formatter)
    formatter.output(self)
  end
end
