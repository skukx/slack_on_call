# frozen_string_literal: true

module Actions
  class DaySelectionService < ReceiveService
    ##
    # @return [Chat::Slack::Message]
    #
    def execute
      Chat::Slack::TimeSelectionMessage.new
    end
  end
end
