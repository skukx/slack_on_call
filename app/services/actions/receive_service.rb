# frozen_string_literal: true

module Actions
  class ReceiveService
    attr_reader :received_action

    def initialize(received_action)
      @received_action = received_action
      @subtask_name = "#{@received_action.callback_id}_service".classify
    end

    ##
    # @return [Chat::Slack::Message]
    #
    def execute
      return handler.new(received_action).execute if handler
      Rails.logger.warn "Handler not defined for callback_id '#{@subtask_name}'"
      Chat::Slack::Message.new text: 'Unknown handler was received.'
    end

    private

    def handler
      Actions.const_get(@subtask_name)
    rescue NameError
      nil
    end
  end
end
