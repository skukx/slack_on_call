# frozen_string_literal: true

module Actions
  class TimeSelectionService < ReceiveService
    ##
    # @return [Chat::Slack::Message]
    #
    def execute
      schedule_params = build_schedule_params
      return Chat::Slack::UserSelectionMessage.new if schedule_params.nil?

      user, _schedule = Schedules::CreateService.new(schedule_params).execute
      user.output_schedule(Chat::Slack::ScheduleFormatter.new)
    end

    private

    def build_schedule_params
      return if (user_selection || day_selection).nil?
      { slack_id: user_value, day: day_value, time: time_value }
    end

    def user_selection
      received_action.sibling(Chat::Slack::UserSelectionAttachment::CALLBACK_ID)
    end

    def user_value
      user_selection.action('user').selected_options.first.value
    end

    def day_selection
      received_action.sibling(Chat::Slack::DaySelectionAttachment::CALLBACK_ID)
    end

    def day_value
      day_selection.action('day').selected_options.first.value
    end

    def time_value
      received_action.action('time').value
    end
  end
end
