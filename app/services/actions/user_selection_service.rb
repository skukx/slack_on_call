# frozen_string_literal: true

module Actions
  class UserSelectionService < ReceiveService
    ##
    # @return [Chat::Slack::Message]
    #
    def execute
      Chat::Slack::DaySelectionMessage.new
    end
  end
end
