# frozen_string_literal: true

class BaseService
  attr_reader :params, :current_user

  def initialize(params = {}, current_user = nil)
    @params = params.dup
    @current_user = current_user
  end
end
