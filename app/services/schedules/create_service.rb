# frozen_string_literal: true

module Schedules
  class CreateService < BaseService
    ##
    # Creates user if not already exists and then creates a new schedule if
    # one does not already exist.
    # @example Obtain user and schedule
    #   user, schedule = service.execute
    #
    # @return [Array<User, Schedule>]
    #
    def execute
      User.transaction do
        @user = User.find_or_create_by!(slack_id: params[:slack_id])
        @schedule = @user.schedules.find_or_create_by!(
          day_of_week: params[:day],
          shift: params[:time]
        )
      end

      [@user, @schedule]

      # @todo Notify user they're schedule has been updated
    end
  end
end
