# frozen_string_literal: true

module Schedules
  class OutputService < BaseService
    def execute
      command = Chat::Slack::UserDayText.new(params[:text])
      return Schedule.output_scheduled_users(command.day) if command.day?

      formatter = SlackMessage::UserSchedulesFormatter.new
      user = Users::FindFromSlackService.new(params, current_user).execute
      user.output_schedule(formatter)
    end
  end
end
