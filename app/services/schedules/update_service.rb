# frozen_string_literal: true

module Schedules
  class UpdateService < BaseService
    def execute
      create_usergroup unless on_call_usergroup
      update_usergroup_users if on_call_users
    end

    private

    def slack_client
      @client ||= Slack::Web::Client.new
    end

    def on_call_usergroup
      @usergroup ||= slack_client.usergroups_list.usergroups.detect do |usergroup|
        usergroup.handle.eql? I18n.t('slack.usergroups.handle')
      end
    end

    def on_call_users
      return @on_call_users if @on_call_users

      wday = Date::DAYNAMES[Date.current.wday].downcase
      shift = Time.current.noon > Time.current ? 'am' : 'pm'
      @on_call_users = User.
        joins(:schedules).
        where(schedules: { day_of_week: wday, shift: shift }).
        distinct
    end

    def create_usergroup
      slack_client.usergroups_create(
        name: I18n.t('slack.usergroups.name'),
        handle: I18n.t('slack.usergroups.handle')
      )
    end

    def update_usergroup_users
      slack_client.usergroups_users_update(
        usergroup: on_call_usergroup.id,
        users: on_call_users.pluck(:slack_id).join(',')
      )
    end
  end
end
