# frozen_string_literal: true

module Users
  class FindFromSlackService < BaseService
    ##
    # Finds
    def execute
      command = Chat::Slack::Text.new(params[:text])
      mentioned_user = command.mentioned_users.first
      return @current_user if mentioned_user.nil?

      User.find_by(slack_id: mentioned_user.id[1..-1])
    end
  end
end
