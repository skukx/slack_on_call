# frozen_string_literal: true

Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :api do
    %w[temporize].each do |action|
      match "/callbacks/#{action}", controller: 'callbacks', action: action, via: %i[get post]
    end
    namespace :slack do
      resources :actions, only: [] do
        collection do
          post :add_on_call_schedule
        end
      end

      resources :commands, only: [] do
        collection do
          post :actions
          post :add_on_call
          post :on_call_schedule
          post :remove_on_call
        end
      end
    end
  end
end
