# frozen_string_literal: true

class CreateUser < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :slack_id
      t.string :slack_name
      t.timestamps
    end
  end
end
