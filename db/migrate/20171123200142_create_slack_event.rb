# frozen_string_literal: true

class CreateSlackEvent < ActiveRecord::Migration[5.1]
  def change
    create_table :slack_events do |t|
      t.json :payload
      t.timestamps
    end
  end
end
