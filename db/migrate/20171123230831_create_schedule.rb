# frozen_string_literal: true

class CreateSchedule < ActiveRecord::Migration[5.1]
  def change
    create_table :schedules do |t|
      t.references :user
      t.integer :day_of_week, default: 0
      t.integer :shift, default: 0
      t.timestamps
    end
  end
end
